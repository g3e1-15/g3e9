//GUIA 3 EJERCICIO 9
//En dos vectores se tienen los goles de dos equipos en diferentes partidos que tuvieron entre si.
//En la posici�n 0 del vector equipo A est�n los goles de un partido por ese equipo y en la misma posici�n del vector equipo B est�n los goles del otro equipo en ese mismo partido.
//En la posici�n 1 de ambos vectores tendremos los goles para otro partido, etc.
//Se pide permitir que el usuario cargue los datos y mostrar cu�ntos empates han ocurrido.

#include <stdio.h>

int main()
{
  int A[5];
  int B[5];
  int i ;
  int contador;

printf("Ingrese la cantidad de goles en los partidos del equipo A:\n");

for(i=0; i<=4; i++)
  {
      scanf("%d",&A[i]);
  }

printf("Ingrese la cantidad de goles en los partidos del equipo B:\n");

for(i=0; i<=4; i++)
  {
      scanf("%d",&B[i]);
  }

contador = 0;

for(i=0; i<=4; i++)
  {
      if(A[i] == B[i])
      {
          contador++;
      }
  }

printf("\nLos equipos A y B empataron en %d partidos",contador);

}
